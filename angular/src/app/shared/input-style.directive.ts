import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appInputStyle]'
})
export class InputStyleDirective {

  constructor(private _el: ElementRef, private _render: Renderer2) {

  }

  @HostListener('keyup')
  inputCheck = ()  =>  {
    this._el.nativeElement.className = this._el.nativeElement.value ? 'form-control' : 'form-control empty';
  }

}

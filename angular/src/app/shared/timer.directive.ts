import { Directive, Renderer2, ElementRef } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: '[appTimer]'
})
export class TimerDirective {
  private paragraph;

  constructor(private _el: ElementRef, private _render: Renderer2) {
    this.paragraph = this._render.createElement('div');
    setInterval(() => {
     $(this.paragraph).text( new Date().toLocaleString());
    }, 1000);
    $(this.paragraph).addClass('timer');
    this._render.appendChild(this._el.nativeElement, this.paragraph);
  }
}

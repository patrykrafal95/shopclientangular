import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appChecked]'
})
export class CheckedDirective {

  constructor(private _el: ElementRef, private _render: Renderer2) {
    const li = this._el.nativeElement;
    console.log('element');
    console.log(li);
      _render.setStyle(li, 'list-style-image', 'url(/assets/check.png)');
  }

}

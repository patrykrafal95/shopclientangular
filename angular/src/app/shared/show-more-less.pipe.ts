import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showMoreLess'
})
export class ShowMoreLessPipe implements PipeTransform {
  transform(value: string, length?: number): string {
    console.log('rozmiar ' + length);
    return  value.length > 100 ? value.slice(0, length) : value;
  }
}

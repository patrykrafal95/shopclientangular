import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective {

  constructor(private _: ElementRef, private _render: Renderer2) {

  }

}

import {
  Input,
  Directive,
  ElementRef,
  Renderer2,
  HostListener
} from '@angular/core';

@Directive({
  selector: '[appDate]'
})
export class DateDirective {
  @Input()
  private date: Date;
  private paragraph;

  constructor(private _el: ElementRef, private _render: Renderer2) {
    this.paragraph = _render.createElement('div');
  }

  @HostListener('mouseenter')
  mouseHover(event: Event) {
    console.log('date ' + this.date);
    this.paragraph.innerHTML = this.date.toLocaleString();
    this._render.appendChild(this._el.nativeElement, this.paragraph);
  }

  @HostListener('mouseleave')
  mouseLeave(event: Event) {
    this._render.removeChild(this._el.nativeElement, this.paragraph);
  }
}

import { Directive, Input, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appShowIp]'
})
export class ShowIpDirective {

  @Input()
  ip: number;

  private _ipDiv;

  constructor(private _ne: ElementRef, private _render: Renderer2) {
    this._ipDiv = this._render.createElement('div');
  }

  @HostListener('mouseenter')
  showIpHover(event: Event) {
    console.log(this.ip);
    this._ipDiv.className = 'showIp';
    this._ipDiv.innerHTML = this.ip;
    this._render.appendChild(this._ne.nativeElement, this._ipDiv);
  }

  @HostListener('mouseleave')
  showIpLeave() {
    this._render.removeChild(this._ne.nativeElement, this._ipDiv);
  }

}

import { TaskService } from './../service/task.service';
import { Component, OnInit } from '@angular/core';
import { ITask } from '../model/task';

@Component({
  selector: 'app-done-tasks',
  templateUrl: './done-tasks.component.html',
  styleUrls: ['./done-tasks.component.scss']
})
export class DoneTasksComponent implements OnInit {
  private listDoneTasks: Array<ITask> = [];

  constructor(private _tasksService: TaskService) {
    _tasksService.getDoneListTasksObs().subscribe(d => {
      this.listDoneTasks = d;
    });
  }

  ngOnInit() {}

  getDoneTasks = () => this.listDoneTasks;

  removeDoneTask = (i) => {this._tasksService.removeDoneTask(i); console.log('kasowanie'); };

}

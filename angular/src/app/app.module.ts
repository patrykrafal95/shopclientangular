import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AddTaskComponent } from './add-task/add-task.component';
import { ListOfTasksComponent } from './list-of-tasks/list-of-tasks.component';
import { TaskService } from './service/task.service';
import { DoneTasksComponent } from './done-tasks/done-tasks.component';
import { CheckedDirective } from './shared/checked.directive';
import { ColorDirective } from './shared/color.directive';
import { DateDirective } from './shared/date.directive';
import { TransformTaskPipe } from './shared/transform-task.pipe';
import { SortPipe } from './shared/sort.pipe';
import { HttpClientComponent } from './http-client/http-client.component';
import { HttpService } from './service/http.service';
import { ShowIpDirective } from './shared/show-ip.directive';
import { ShowMoreLessPipe } from './shared/show-more-less.pipe';
import { InputStyleDirective } from './shared/input-style.directive';
import { TimerDirective } from './shared/timer.directive';
import { AddPostComponent } from './add-post/add-post.component';

import {ToastrModule} from 'ngx-toastr';
import { MessageService } from './service/message.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Config } from 'src/app/model/config';
import { CONFIG } from './model/config';

const config: Config = {
  apiUrl: 'http://localhost:13605/api/Home'
};

@NgModule({
  declarations: [
    AppComponent,
    AddTaskComponent,
    ListOfTasksComponent,
    DoneTasksComponent,
    CheckedDirective,
    ColorDirective,
    DateDirective,
    TransformTaskPipe,
    SortPipe,
    HttpClientComponent,
    ShowIpDirective,
    ShowMoreLessPipe,
    InputStyleDirective,
    TimerDirective,
    AddPostComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  providers: [
    TaskService,
    HttpService,
    HttpClientComponent,
    MessageService,
    {provide: CONFIG, useValue: config}
   ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ITask } from '../model/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private listTasks: Array<ITask> = [];
  private listDoneTasks: Array<ITask> = [];

  private emptyInputError = '';

  private listTasksObs = new BehaviorSubject<Array<ITask>>([]);
  private listDoneTasksObs = new BehaviorSubject<Array<ITask>>([]);

  private emptyInputErrorObs = new BehaviorSubject<string>('');

  constructor() {}

  addTask = (task: ITask) => {
    console.log(task.name);
    if (task.name !== '' && task.name !== undefined) {
      this.listTasks.push(task);
      this.emptyInputError = '';
      console.log('non empty');
    } else {
      this.emptyInputError = 'empty';
      console.log('empty');
    }
    this.emptyInputErrorObs.next(this.emptyInputError);
    this.listTasksObs.next(this.listTasks);
    const tasks = this.listTasksObs.getValue();
    console.log('zadania ' + tasks.length);
  }

  gettListTasksObs = () => this.listTasksObs.asObservable();

  getInputEmptyObs = () => this.emptyInputErrorObs.asObservable();

  removeTask = i => {
    this.listTasks.splice(i, 1);
    this.listTasksObs.next(this.listTasks);
  }

  getElementById = (i, tab) => {
    for (let index = 0; index < tab.length; index++) {
      if (i === index) {
        return tab[index];
      }
    }
  }

  addToDone = i => {
    const doneT: ITask = ({name: this.getElementById(i, this.listTasks).name, create: new Date() });
    this.listDoneTasks.push(doneT);
    this.listDoneTasksObs.next(this.listDoneTasks);
    this.removeTask(i);
  }

  removeDoneTask = i => {
    this.listDoneTasks.splice(i, 1);
    this.listDoneTasksObs.next(this.listDoneTasks);
    console.log('kasowanie');
  }

  getDoneListTasksObs = () => this.listDoneTasksObs.asObservable();
}

import { post } from 'selenium-webdriver/http';
import { Observable } from 'rxjs/internal/Observable';
import { BehaviorSubject } from 'rxjs';
import { Post } from './../model/post';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config, CONFIG } from 'src/app/model/config';


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _hc: HttpClient, @Inject(CONFIG) private config: Config  ) {

  }

  getPosts() {
    return this._hc.get<Post[]>(`${this.config.apiUrl}`);
  }

  addPosts(p: Post) {
    return this._hc.post(`${this.config.apiUrl}/addPost`, p);
  }

  removePost(p: Post) {
    return this._hc.get(`${this.config.apiUrl}/removePost/${p.id}`);
  }

}

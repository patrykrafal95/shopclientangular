import { Config } from 'src/app/model/config';
import { InjectionToken } from '@angular/core';
export interface Config {
  apiUrl: string;
}

export const CONFIG = new InjectionToken<Config>('app.config');

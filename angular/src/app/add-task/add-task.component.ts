import { ITask } from './../model/task';
import { Component, OnInit } from '@angular/core';
import { TaskService } from '../service/task.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {

  task: string;
  emptyInputError = '';

  constructor(private _taskService: TaskService) {
    _taskService.getInputEmptyObs().subscribe(i => {
      this.emptyInputError = i;
    });
  }

  ngOnInit() {
  }

  addTask = () => {
    const task: ITask = ({name: this.task, create: new Date()});
    this._taskService.addTask(task); this.task = '';
  }

}

import { Config } from './../model/config';
import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { HttpService } from '../service/http.service';
import { Post } from '../model/post';
import { post } from 'selenium-webdriver/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { CONFIG } from '../model/config';
import { MessageService } from '../service/message.service';

@Component({
  selector: 'app-http-client',
  templateUrl: './http-client.component.html',
  styleUrls: ['./http-client.component.scss']
})
export class HttpClientComponent implements OnInit {

  posts: Post[];

  constructor(public _hs: HttpService, private _message: MessageService) {
  }

  ngOnInit() {
    this.refresh();
  }

  postsCount = () => this.posts.length;

  private refresh = () => {
    this._hs.getPosts().subscribe(p => {
      this.posts = p;
    });
  }

  removePost(p: Post) {
    console.log('kasowanie');
    this._hs.removePost(p).subscribe(() => {
      this.refresh();
      this._message.success('Skasowano');
    }, error => {
        this._message.error('Nie udało się skasować');
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { TaskService } from '../service/task.service';
import { ITask } from '../model/task';

@Component({
  selector: 'app-list-of-tasks',
  templateUrl: './list-of-tasks.component.html',
  styleUrls: ['./list-of-tasks.component.scss']
})
export class ListOfTasksComponent implements OnInit {

  private listTasks: Array<ITask> = [];

  constructor(private _taskService: TaskService) {
    _taskService.gettListTasksObs().subscribe(t => {
      this.listTasks = t.slice();
    });
  }

  ngOnInit() {
  }

  getListTasks = () => this.listTasks;

  removeTask  = (i) => this._taskService.removeTask(i);

  addToDone = (i) => this._taskService.addToDone(i);

}

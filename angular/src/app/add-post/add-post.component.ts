import { MessageService } from './../service/message.service';
import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { Post } from '../model/post';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  post: string;
  private emptyInputError: string;

  constructor(private _hs: HttpService, private _ms: MessageService) {}

  ngOnInit() {}
  addPost = () => {
    console.log(this.post);
    if (this.post !== '' && this.post !== undefined) {
      const p: Post = {
        body: this.post,
        id: null,
        title: 'fdfdsfsd',
        userId: 1
      };
      this.emptyInputError = '';
      this._hs.addPosts(p).subscribe();
      this._ms.success('Dodano');
    } else {
      this.emptyInputError = 'empty';
      this._ms.error('Pole input jest puste');
    }
    this.post = '';
  }

  errorInputClass = () => this.emptyInputError;
}
